package io.github.yangyouwang.common.constant;

/**
 * 通用常量
 * @author yangyouwang
 */
public interface Constants {
    /**
     * 超级管理员
     */
    Long ADMIN_USER = 1L;
    /**
     * 是否启用 是
     */
    String ENABLED_YES = "Y";
    /**
     * 是否启用 否
     */
    String ENABLED_NO = "N";
}
