package io.github.yangyouwang.crud.act.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.yangyouwang.crud.act.model.ActReModel;

/**
 * @author yangyouwang
 * @title: ActReModelMapper
 * @projectName crud
 * @description: 流程模型
 * @date 2021/4/10下午2:01
 */
public interface ActReModelMapper extends BaseMapper<ActReModel> {
}
