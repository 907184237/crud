package io.github.yangyouwang.crud.system.mapper;

import io.github.yangyouwang.common.base.CrudBaseMapper;
import io.github.yangyouwang.crud.system.model.SysTask;

/**
 * task任务Mapper
 * @author yangyouwang
 */
public interface SysTaskMapper extends CrudBaseMapper<SysTask> {
}
