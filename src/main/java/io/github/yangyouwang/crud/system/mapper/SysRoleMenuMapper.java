package io.github.yangyouwang.crud.system.mapper;

import io.github.yangyouwang.common.base.CrudBaseMapper;
import io.github.yangyouwang.crud.system.model.SysRoleMenu;

/**
 * @author yangyouwang
 * @title: SysRoleMenuMapper
 * @projectName crud
 * @description: 角色关联菜单Mapper
 * @date 2021/4/19:59 AM
 */
public interface SysRoleMenuMapper extends CrudBaseMapper<SysRoleMenu> {
}