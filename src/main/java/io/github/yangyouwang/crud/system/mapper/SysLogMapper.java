package io.github.yangyouwang.crud.system.mapper;

import io.github.yangyouwang.common.base.CrudBaseMapper;
import io.github.yangyouwang.crud.system.model.SysLog;

/**
 * @author yangyouwang
 * @title: SysLogMapper
 * @projectName crud
 * @description: 系统日志Mapper
 * @date 2021/4/19:59 AM
 */
public interface SysLogMapper extends CrudBaseMapper<SysLog> {
}