package io.github.yangyouwang.crud.system.mapper;


import io.github.yangyouwang.common.base.CrudBaseMapper;
import io.github.yangyouwang.crud.system.model.SysUserRole;

/**
 * @author yangyouwang
 * @title: SysUserRoleMapper
 * @projectName crud
 * @description: 用户关联角色Mapper
 * @date 2021/4/19:59 AM
 */
public interface SysUserRoleMapper extends CrudBaseMapper<SysUserRole> {

}